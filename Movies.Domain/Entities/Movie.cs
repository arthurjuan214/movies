﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain.Entities;

public class Movie : Entity
{
    public string Name { get; set; }
    public string Description { get; set; }
    public double Duration { get; set; }
    public TimeOnly TimeStart {  get; set; }
    public TimeOnly TimeEnd { get; set; }

}

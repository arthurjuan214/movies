﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain.Entities;

public class Location : Entity
{
    public string Name { get; set; }    
    public Address Address { get; set; }
    public TimeOnly Opened { get; set; }
    public TimeOnly Closed { get; set; }
    public List<Cine> Cines { get; set; }


    public bool AddCine(Cine cine)
    {
        Cines ??= new List<Cine>();
        if(cine !=null)
        {
            foreach(var c in Cines)
            {
                if (cine.Name == c.Name)
                    return false;
            }
            Cines.Add(cine);
            return true;
        }
        return false;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain.Entities;

public class Seat : Entity
{
    public int Number { get; set; }
    public string Type { get; set; }
}

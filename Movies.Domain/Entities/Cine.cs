﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain.Entities;

public class Cine : Entity
{
    public string Name { get; set; }
    public int Identifier { get; set; }
    public List<Room> Rooms { get; set; }
    public TimeOnly Opened { get; set; }
    public TimeOnly Closed { get; set; }
    public List<Staff> Staff { get; set; }

    public void AddRoom(Room room)
    {
        Rooms ??= new List<Room>();
        if(room != null)
        {
            Rooms.Add(room);
        }
    }
}

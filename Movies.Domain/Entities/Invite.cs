﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain.Entities;

public class Invite : Entity
{
    public Section Section{ get; set; }
    public double Price { get; set; }   
    public TimeOnly Hour { get; set; }
    public Room Room { get; set; }
    public Customer Customer { get; set; }
    public Seat Seat { get; set; }


    public bool SellInvite(Customer customer, Section section, Seat seat)
    {
        if(customer != null && section != null && seat != null)
        {
            if(Customer == null)
            {
                Customer = customer;
                var selled = section.SellSeat(seat);
                if( !selled)
                {
                    return false;
                }

            }
            return false;
        }
        return false;
    }

}

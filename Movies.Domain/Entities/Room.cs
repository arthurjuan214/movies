﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain.Entities;

public class Room : Entity
{
    public List<Seat> SeatList { get; set; }
    public List<Seat> TotalSeats { get; set; }
    public int SeatCount { get; set;}
    public List<Section> Sections{ get; set; }
    public Cine Cine { get; set; }

    public bool AddSection(Section section)
    {
        Sections ??= new List<Section>();
        if(section != null)
        {
            
            if(section.Start <= Cine.Closed 
                && section.Start >=  Cine.Opened 
                && section.End <= Cine.Closed
                && section.End >= Cine.Opened) 
            {
                if(!IsOverlapping(section))
                { 
                    Sections.Add(section);
                    return true;
                }
                return false;
            }
        }
        return false;
    }
    public bool RemoveSection(Section section)
    {
        if(section!= null)
        {
            var sectionDoestNotExists = Sections.Where(x => x.Id == section.Id).FirstOrDefault();
            if(sectionDoestNotExists != null)
            {
                return false;
            }
            Sections.Remove(section);
            return true;
        }
        return false;
    }

    public bool SellSeat(Seat seat)
    {
        if(seat != null)
        {
            var seatExists = SeatList.Where(x => x.Id == seat.Id).FirstOrDefault();
            if(seatExists != null)
            {

                var isDisponible = TotalSeats.Where(x => x.Id == seat.Id).FirstOrDefault();
                if(isDisponible != null)
                {
                    TotalSeats.Remove(seat);
                    SeatCount = TotalSeats.Count();
                    return true;
                }
                return false;
            }

            return false;

        }
        return false;
    }

    private bool IsOverlapping(Section section)
    {
        foreach(var s in Sections)
        {
            if(s.Start < section.Start && section.Start < section.Start.AddHours(2))
            {
                return true;
            }

        }
        return false;
    }
    

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Domain.Entities;

public class Section : Entity
{
    public Movie Movie { get; set; }
    public TimeOnly Start { get; set; }
    public TimeOnly End { get; set; }
    public Room Room { get; set; }
    public List<Seat> SeatList { get; set; }
    public List<Invite> Invites { get; set; }

    public bool SellSeat(Seat seat)
    {
        if (seat != null)
        {
            var seatExists = SeatList.Where(x => x.Id == seat.Id).FirstOrDefault();
            if (seatExists != null)
            {

                var isDisponible = Room.TotalSeats.Where(x => x.Id == seat.Id).FirstOrDefault();
                if (isDisponible != null)
                {
                    SeatList.Remove(seat);
                    return true;
                }
                return false;
            }

            return false;

        }
        return false;
    }
}
